# storage/nvdimm/fsdax_device_add

Storage: nvdimm test adding dax device works and non-dax device should failed

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
```bash
bash ./runtest.sh
```
