From 93b28ee69d9243501d4e255534d086936cbec521 Mon Sep 17 00:00:00 2001
From: Andrea Cervesato <andrea.cervesato@suse.com>
Date: Mon, 22 Jul 2024 16:28:41 +0200
Subject: [PATCH 2/6] Add cachestat01 test

This test verifies that cachestat() syscall is properly counting
cached pages written inside a file. If storage device synchronization
is requested, test will check if the number of dirty pages is zero.

Signed-off-by: Andrea Cervesato <andrea.cervesato@suse.com>
Reviewed-by: Cyril Hrubis <chrubis@suse.cz>
---
 runtest/syscalls                              |   2 +
 .../kernel/syscalls/cachestat/.gitignore      |   1 +
 testcases/kernel/syscalls/cachestat/Makefile  |  10 ++
 .../kernel/syscalls/cachestat/cachestat.h     |  27 +++++
 .../kernel/syscalls/cachestat/cachestat01.c   | 107 ++++++++++++++++++
 5 files changed, 147 insertions(+)
 create mode 100644 testcases/kernel/syscalls/cachestat/.gitignore
 create mode 100644 testcases/kernel/syscalls/cachestat/Makefile
 create mode 100644 testcases/kernel/syscalls/cachestat/cachestat.h
 create mode 100644 testcases/kernel/syscalls/cachestat/cachestat01.c

diff --git a/runtest/syscalls b/runtest/syscalls
index 27eb9a86b..ce974b317 100644
--- a/runtest/syscalls
+++ b/runtest/syscalls
@@ -62,6 +62,8 @@ capset04 capset04
 
 cacheflush01 cacheflush01
 
+cachestat01 cachestat01
+
 chdir01 chdir01
 chdir01A symlink01 -T chdir01
 chdir04 chdir04
diff --git a/testcases/kernel/syscalls/cachestat/.gitignore b/testcases/kernel/syscalls/cachestat/.gitignore
new file mode 100644
index 000000000..daea1f4be
--- /dev/null
+++ b/testcases/kernel/syscalls/cachestat/.gitignore
@@ -0,0 +1 @@
+cachestat01
diff --git a/testcases/kernel/syscalls/cachestat/Makefile b/testcases/kernel/syscalls/cachestat/Makefile
new file mode 100644
index 000000000..62b00d2f4
--- /dev/null
+++ b/testcases/kernel/syscalls/cachestat/Makefile
@@ -0,0 +1,10 @@
+# SPDX-License-Identifier: GPL-2.0-or-later
+# Copyright (C) 2024 SUSE LLC Andrea Cervesato <andrea.cervesato@suse.com>
+
+top_srcdir		?= ../../../..
+
+include $(top_srcdir)/include/mk/testcases.mk
+
+LDLIBS += -lrt
+
+include $(top_srcdir)/include/mk/generic_leaf_target.mk
diff --git a/testcases/kernel/syscalls/cachestat/cachestat.h b/testcases/kernel/syscalls/cachestat/cachestat.h
new file mode 100644
index 000000000..efce6dc7f
--- /dev/null
+++ b/testcases/kernel/syscalls/cachestat/cachestat.h
@@ -0,0 +1,27 @@
+/* SPDX-License-Identifier: GPL-2.0-or-later */
+/*
+ * Copyright (C) 2024 SUSE LLC Andrea Cervesato <andrea.cervesato@suse.com>
+ */
+
+#ifndef CACHESTAT_H__
+#define CACHESTAT_H__
+
+#include "tst_test.h"
+#include "lapi/mman.h"
+
+static inline void print_cachestat(struct cachestat *cs)
+{
+	tst_res(TDEBUG,
+		"nr_cache=%lu "
+		"nr_dirty=%lu "
+		"nr_writeback=%lu "
+		"nr_evicted=%lu "
+		"nr_recently_evicted=%lu",
+		cs->nr_cache,
+		cs->nr_dirty,
+		cs->nr_writeback,
+		cs->nr_evicted,
+		cs->nr_recently_evicted);
+}
+
+#endif
diff --git a/testcases/kernel/syscalls/cachestat/cachestat01.c b/testcases/kernel/syscalls/cachestat/cachestat01.c
new file mode 100644
index 000000000..f3730d773
--- /dev/null
+++ b/testcases/kernel/syscalls/cachestat/cachestat01.c
@@ -0,0 +1,107 @@
+// SPDX-License-Identifier: GPL-2.0-or-later
+/*
+ * Copyright (C) 2024 SUSE LLC Andrea Cervesato <andrea.cervesato@suse.com>
+ */
+
+/*\
+ * [Description]
+ *
+ * This test verifies that cachestat() syscall is properly counting cached pages
+ * written inside a file. If storage device synchronization is requested, test
+ * will check if the number of dirty pages is zero.
+ *
+ * [Algorithm]
+ *
+ * - create a file with specific amount of pages
+ * - synchronize storage device, if needed
+ * - monitor file with cachestat()
+ * - check if the right amount of pages have been moved into cache
+ * - if storage device synchronization is requested, check that dirty pages is
+ *    zero
+ */
+
+#include <stdlib.h>
+#include "cachestat.h"
+
+#define MNTPOINT "mntpoint"
+#define FILENAME MNTPOINT "/myfile.bin"
+
+static int page_size;
+static char *page_data;
+static struct cachestat *cs;
+static struct cachestat_range *cs_range;
+
+static void test_cached_pages(const unsigned int use_sync, const int num_pages)
+{
+	int fd;
+
+	tst_res(TINFO, "%s file synchronization", use_sync ? "Enable" : "Disable");
+	tst_res(TINFO, "Number of pages: %d", num_pages);
+
+	memset(cs, 0, sizeof(struct cachestat));
+
+	fd = SAFE_OPEN(FILENAME, O_RDWR | O_CREAT, 0600);
+
+	for (int i = 0; i < num_pages; i++)
+		SAFE_WRITE(0, fd, page_data, page_size);
+
+	if (use_sync)
+		fsync(fd);
+
+	cs_range->off = 0;
+	cs_range->len = page_size * num_pages;
+
+	memset(cs, 0xff, sizeof(*cs));
+
+	TST_EXP_PASS(cachestat(fd, cs_range, cs, 0));
+	print_cachestat(cs);
+
+	TST_EXP_EQ_LI(cs->nr_cache + cs->nr_evicted, num_pages);
+
+	if (use_sync)
+		TST_EXP_EQ_LI(cs->nr_dirty, 0);
+
+	SAFE_CLOSE(fd);
+	SAFE_UNLINK(FILENAME);
+}
+
+static void run(unsigned int use_sync)
+{
+	for (int i = 0; i < 15; i++)
+		test_cached_pages(use_sync, 1 << i);
+}
+
+static void setup(void)
+{
+	page_size = (int)sysconf(_SC_PAGESIZE);
+
+	page_data = SAFE_MALLOC(page_size);
+	memset(page_data, 'a', page_size);
+}
+
+static void cleanup(void)
+{
+	free(page_data);
+}
+
+static struct tst_test test = {
+	.test = run,
+	.tcnt = 2,
+	.setup = setup,
+	.cleanup = cleanup,
+	.needs_tmpdir = 1,
+	.min_kver = "6.5",
+	.mount_device = 1,
+	.mntpoint = MNTPOINT,
+	.all_filesystems = 1,
+	.skip_filesystems = (const char *const []) {
+		"fuse",
+		"tmpfs",
+		NULL
+	},
+	.bufs = (struct tst_buffers []) {
+		{&cs, .size = sizeof(struct cachestat)},
+		{&cs_range, .size = sizeof(struct cachestat_range)},
+		{}
+	},
+};
-- 
2.45.2

